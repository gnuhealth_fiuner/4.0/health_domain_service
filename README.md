
## Servicio de integración con el bus de la Nube Sanitaria Argentina

Como parte de la [Red Nacional de Salud Digital](https://www.argentina.gob.ar/salud/digital/red),
en la Nube de Información Sanitaria (NIS), se definen dominios o
nodos de interconexión.

Se pueden ver [especificaciones técnicas](https://bus.msal.gob.ar/fhir/ar/core/site/index.html) y
[referencias](https://bus.msal.gob.ar/fhir/ar/core/site/sandbox.html)


### Instalación

Se recomienda instalar en primer lugar GNU Health (con su versión de Tryton
correspondiente) en un entorno virtual de Python. Luego instalar las
dependencias, según se indica en el archivo 'requirements.txt'.
Seguir también las indicación del apartado 'Configuración'

Para ejecutar el servidor:

    ./run.py


### Configuración

Es necesario definir la variable SERVICE_KEY en la configuración (archivo
'config.py'). En entorno de producción se recomienda generar una nueva.
Para generar una key, se puede ejecutar desde el intérprete de Python:

    >>> import os
    >>> import base64
    >>> print(base64.urlsafe_b64encode(os.urandom(32)).decode())

Esta misma variable debe ser cargada en el archivo trytond.conf, para
asegurar la comunicación entre Tryton y este servicio.
Añadir lo siguiente en trytond.conf, con los valores apropiados:

    [health_domain]
    url=http://localhost:5000
    service_key = HQHTXuSD0AO9N0uE4UDxofGakq8Zee3DOZkIZVn0nMY=

En el archivo 'config.py' también se deben adaptar las variables de
configuración del servidor Tryton:

    TRYTON_DATABASE = 'gnuhealth'
    TRYTON_CONFIG = '/ruta/configuracion/gnuhealth/trytond.conf'


## Testing

    curl http://localhost:5000/health/users/1

    curl -d '{"user": "admin", "token": "gAAAAABgYx70J8Hkmqz9a4DXx0Kdb-wFCx_d3NdiLaWDdIEK_4W6YnB9AkDfD_tRZM90TxzlK2yknYRQPwe33oI70cG3lL1OaQ==", "id_number": "99232231"}' -H "Content-Type: application/json" -X POST http://localhost:5000/get_by_dni

    curl -d '{"user": "admin", "token": "gAAAAABlqRS8CX0x0QEU11nNvx5Jn3ACfgQ93UNHQ9UcgduVJjQz8qfDpXbtZQ2DuIMbhsen1t1SCMbyzvAEZObaVZUv3jUb3Q==", "resource": "{\"resourceType\": \"Parameters\", \"id\": \"mymatch\", \"parameter\": [{\"name\": \"resource\", \"resource\": {\"resourceType\": \"Patient\", \"identifier\": [{\"use\": \"usual\", \"system\": \"http://www.renaper.gob.ar/dni\", \"value\": \"23327765\"}], \"name\": [{\"_family\": {\"extension\": [{\"url\": \"http://hl7.org/fhir/StructureDefinition/humanname-fathers-family\", \"valueString\": \"MARTINEZ\"}]}, \"given\": [\"JULIAN\"]}], \"gender\": \"male\", \"birthDate\": \"1973-06-13\"}}, {\"name\": \"count\", \"valueInteger\": 5}]}"}' -H "Content-Type: application/json" -X POST http://localhost:5000/match


## Basado en

* [Tryton](http://www.tryton.org/)

## Licencia

Ver el archivo LICENSE

## Copyright

Ver el archivo COPYRIGHT
