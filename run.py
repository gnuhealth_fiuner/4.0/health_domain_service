#!/usr/bin/env python3

import os
from server import create_app

# For production mode:
# export DOMAIN_CONFIG_SETTINGS='config.ProductionConfig'
config_settings = os.getenv('DOMAIN_CONFIG_SETTINGS')
app = create_app(config_settings)

if __name__ == '__main__':
    app.run()
