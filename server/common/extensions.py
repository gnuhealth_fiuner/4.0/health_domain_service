from flask_tryton import Tryton
from flask_jwt_extended import JWTManager, create_access_token
from flask_caching import Cache

__all__ = ['tryton', 'jwt', 'create_access_token', 'cache']

tryton = Tryton()
jwt = JWTManager()
cache = Cache()
