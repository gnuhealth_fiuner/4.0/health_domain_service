from flask_restful import Resource
from server.common.extensions import tryton

__all__ = ['User']


class User(Resource):
    @tryton.transaction()
    def get(self, user):
        '''Read interaction'''
        return {'Usuario: ': user.name}
