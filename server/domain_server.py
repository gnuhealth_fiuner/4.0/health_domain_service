import requests
import json
from flask import Flask, jsonify, request
from flask_restful import Api
from cryptography.fernet import Fernet
from server.common.error_handling import ObjectNotFound, AppErrorBaseClass
from server.common.extensions import tryton, jwt, create_access_token, cache


def create_app(config=None):
    app = Flask(__name__, template_folder='templates')
    if config is not None:
        app.config.from_object(config)
    else:
        app.config.from_object('config.DevelopmentConfig')

    if app.config['TEST_MODE']:
        bus_url = 'https://bus-test.msal.gob.ar'
        jwt_claims = {
            'iss': app.config['DOMAIN_URI'],
            'sub': 'Ministerio de Salud de la Nación',
            'aud': 'https://bus-test.msal.gob.ar/bus-auth/auth',
            }
    else:
        bus_url = 'https://federador.msal.gob.ar'
        jwt_claims = {
            'iss': app.config['DOMAIN_URI'],
            'sub': 'Ministerio de Salud de la Nación',
            'aud': 'https://federador.msal.gob.ar/bus-auth/auth',
            }
    patient_path = '/masterfile-federacion-service/fhir/Patient'

    with app.app_context():
        tryton.init_app(app)
        jwt.init_app(app)
        cache.init_app(app)

        # Capture 404 errors
        api = Api(app, catch_all_404s=True)
        # Disable strict mode for finishing slash in URL
        app.url_map.strict_slashes = False

        # User = tryton.pool.get('res.user')

        # @tryton.default_context
        # def default_context():
        #     return User.get_preferences(context_only=True)

        # @app.route('/user/<record("res.user"):user>')
        # @tryton.transaction()
        # def user(user):
        #     return '%s, Context: %s' % (user.name, default_context())

        @app.route('/')
        @tryton.transaction()
        def hello():
            User = tryton.pool.get('res.user')
            user, = User.search([('login', '=', 'admin')])
            return '%s, Hello World!\n' % user.name

        @tryton.transaction()
        @cache.cached(timeout=150, key_prefix='create_token')
        def create_token(username, usertoken):
            key = app.config['SERVICE_KEY']
            f = Fernet(key)
            usertoken = usertoken.encode()
            message = f.decrypt(usertoken).decode()
            User = tryton.pool.get('res.user')
            user = User.search([('login', '=', username)])
            if not user or user[0].active is False:
                return jsonify({"msg": "Username is not valid"}), 401
            # Encrypted message should be the same username
            if username != message:
                return jsonify({"msg": "Bad username or password"}), 401
            jwt_claims['name'] = user[0].name
            jwt_claims['role'] = 'Health User'
            jwt_claims['ident'] = str(user[0].id)
            return create_access_token(identity=username,
                additional_claims=jwt_claims)

        @cache.cached(timeout=150, key_prefix='request_token')
        def request_token(access_token):
            payload = json.dumps({
                'grantType': 'client_credentials',
                'scope': 'Patient/*.read,Patient/*.write',
                'clientAssertionType':
                    'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
                'clientAssertion': access_token
                })
            headers = {
                'Content-Type': 'application/json'
                }
            url = bus_url + '/bus-auth/auth'
            return requests.request('POST', url, headers=headers,
                    data=payload)

        def validate_token(access_token):
            payload = json.dumps({
                'accessToken': access_token
                })
            headers = {
                'Content-Type': 'application/json'
                }
            url = bus_url + '/bus-auth/tokeninfo'
            return requests.request('POST', url, headers=headers,
                    data=payload)

        @app.route('/get_by_dni', methods=['POST'])
        def get_by_dni():
            username = request.json.get('user', None)
            usertoken = request.json.get('token', None)
            id_number = request.json.get('id_number', None)

            req_token = create_token(username, usertoken)
            resp = request_token(req_token)
            resp_text = json.loads(resp.text)
            if resp.status_code == 200 and 'accessToken' in resp_text:
                access_token = resp_text['accessToken']
                # --Token validation
                # resp = validate_token(access_token)
                # if resp.status_code != 200:
                #     print(f'Status code: {resp.status_code}: {resp.text}')
                headers = {
                    'Authorization': 'Bearer ' + access_token
                    }
                url = bus_url + patient_path
                url += '?identifier=http://www.renaper.gob.ar/dni|'
                url += str(id_number)
                resp = requests.request(
                    'GET', url, headers=headers, data={})
                print(f'Status code: {resp.status_code}: {resp.text}')
            else:
                print(f'REQ: {resp_text}')
            if resp.text:
                return jsonify(resp.text)
            else:
                return None

        @app.route('/match', methods=['POST'])
        def match():
            username = request.json.get('user', None)
            usertoken = request.json.get('token', None)
            data = request.json.get('data', None)

            req_token = create_token(username, usertoken)
            resp = request_token(req_token)
            if resp.status_code != 200:
                print(f'REQ: {resp.text}')
                return None
            resp_text = json.loads(resp.text)
            if resp.status_code == 200 and 'accessToken' in resp_text:
                access_token = resp_text['accessToken']
                # --Token validation
                # resp = validate_token(access_token)
                # if resp.status_code != 200:
                #     print(f'Status code: {resp.status_code}: {resp.text}')
                headers = {
                    'Authorization': 'Bearer ' + access_token,
                    'Content-Type': 'application/json'
                    }
                url = bus_url + patient_path + '/$match'
                resp = requests.request(
                    'POST', url, headers=headers, data=data)
                print(f'Status code: {resp.status_code}: {resp.text}')
            else:
                print(f'REQ: {resp_text}')
            if resp.text:
                return jsonify(resp.text)
            else:
                return None

        @app.route('/federate', methods=['POST'])
        def federate():
            username = request.json.get('user', None)
            usertoken = request.json.get('token', None)
            data = request.json.get('data', None)

            req_token = create_token(username, usertoken)
            resp = request_token(req_token)
            if resp.status_code != 200:
                print(f'REQ: {resp.text}')
                return None
            resp_text = json.loads(resp.text)
            if resp.status_code == 200 and 'accessToken' in resp_text:
                access_token = resp_text['accessToken']
                # --Token validation
                # resp = validate_token(access_token)
                # if resp.status_code != 200:
                #     print(f'Status code: {resp.status_code}: {resp.text}')
                headers = {
                    'Authorization': 'Bearer ' + access_token,
                    'Content-Type': 'application/json'
                    }
                url = bus_url + patient_path
                resp = requests.request(
                    'POST', url, headers=headers, data=data)
                print(f'Status code: {resp.status_code}: {resp.text}')
                return [resp.status_code, resp.reason]
            else:
                print(f'REQ: {resp_text}')
            if resp.text:
                return jsonify(resp.text)
            else:
                return None

        from server.health.resources import User
        api.add_resource(User, '/health/users/<record("res.user"):user>',
            endpoint='health_user')

        # Registra manejadores de errores personalizados
        # register_error_handlers(app)

    return app


def register_error_handlers(app):
    @app.errorhandler(Exception)
    def handle_exception_error(e):
        print(e)
        return jsonify({'msg': 'Internal server error'}), 500

    @app.errorhandler(405)
    def handle_405_error(e):
        return jsonify({'msg': 'Method not allowed'}), 405

    @app.errorhandler(403)
    def handle_403_error(e):
        return jsonify({'msg': 'Forbidden error'}), 403

    @app.errorhandler(404)
    def handle_404_error(e):
        return jsonify({'msg': 'Not Found error'}), 404

    @app.errorhandler(AppErrorBaseClass)
    def handle_app_base_error(e):
        return jsonify({'msg': str(e)}), 500

    @app.errorhandler(ObjectNotFound)
    def handle_object_not_found_error(e):
        return jsonify({'msg': str(e)}), 404


if __name__ == "__main__":
    app = create_app()
    app.run()
