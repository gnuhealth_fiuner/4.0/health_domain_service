class ProductionConfig(object):
    FLASK_APP = 'entrypoint:server'
    ENV = 'production'
    TEST_MODE = False
    DOMAIN_URI = 'http://www.msal.gov.ar'
    JWT_SECRET_KEY = '7110c8ae51a4b5af97be6534caef90e4bb9bdcb3380af008f90b23a5d1616bf319bc298105da20fe'
    DEBUG = False
    SERVICE_KEY = 'HQHTXuSD0AO9N0uE4UDxofGakq8Zee3DOZkIZVn0nMY='
    TRYTON_DATABASE = 'db_name'
    TRYTON_CONFIG = '/path/to/trytond.conf'
    CACHE_TYPE = 'SimpleCache'


class DevelopmentConfig(object):
    FLASK_APP = 'entrypoint:server'
    ENV = 'development'
    TEST_MODE = True
    DOMAIN_URI = 'https://conn6dummy.org.ar'
    JWT_SECRET_KEY = 'federar'
    DEBUG = True
    SERVICE_KEY = 'HQHTXuSD0AO9N0uE4UDxofGakq8Zee3DOZkIZVn0nMY='
    TRYTON_DATABASE = 'health44'
    TRYTON_CONFIG = '/home/mario/python-envs/proyectos/health_44/etc/trytond.conf'
    CACHE_TYPE = 'SimpleCache'
